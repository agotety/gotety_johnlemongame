﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TrapDoor : MonoBehaviour
{
    public GameObject trap;

    public void OnTriggerEnter()
    {
        trap.GetComponent<Animation>().Play("Trap door");
        //trap.SetActive(false);
        //print("TRAP ENTERED");
    }
}
